import com.typesafe.sbt.packager.docker._

val microserviceName = "microservice-security"

name := microserviceName

version := "1.0.0"

scalaVersion := "2.11.8"

organization := "com.bineer.microservices"

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")

resolvers ++= Seq("Spring milestones" at "https://repo.spring.io/milestone/")

libraryDependencies ++= {
  val MS_KERNEL = "1.0.0"
  Seq(
    "com.scalakka.microservices" %% "microservice-kernel" % MS_KERNEL
  )
}

enablePlugins(JavaAppPackaging)
enablePlugins(sbtdocker.DockerPlugin)

packageName in Docker := "llfrometa/" + microserviceName
version in Docker := sys.props.getOrElse("version", default = "demo")

dockerCommands := Seq(
  Cmd("FROM", "openjdk:alpine"),
  Cmd("RUN", "apk add --update bash && rm -rf /var/cache/apk/*"),
  Cmd("WORKDIR", "/opt/docker"),
  Cmd("ADD", "opt /opt"),
  Cmd("ENTRYPOINT", "bin/" + microserviceName, "-Dconfig.resource=/application-${MSENV}.conf -DmicroserviceName=" + microserviceName + "  -DmyIp=\"$(hostname -i | awk '{ print $1 }')\""),
  ExecCmd("CMD", "bin/" + microserviceName)
)

mainClass in Compile := Some("com.bineer.microservices.security.Main")