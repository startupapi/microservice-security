package com.bineer.microservices.security.util

object RoutePathEnum {

  final val USERS = "users"
  final val LOGIN = "login"
  final val CHANGE_PASSWORD = "change-password"
}