package com.bineer.microservices.security.application

import com.google.inject.{AbstractModule, Provider}
import com.scalakka.microservices.kernel.annotation.{Named, Names}
import com.scalakka.microservices.kernel.application.ServiceProvider
import com.scalakka.microservices.kernel.util.ActorFactoryType.ActorFactory
import com.bineer.microservices.security.SystemModule
import com.bineer.microservices.security.application.services.UserService
import net.codingwell.scalaguice.ScalaModule

package object ioc {

  object ApplicationModule {

    @Named(classOf[UserService])
    class UserServiceProvider() extends Provider[ActorFactory] with ServiceProvider {
      override def get(): ActorFactory = () => SystemModule.system.actorOf(UserService.props, generateName)
    }

  }

  class ApplicationModule extends AbstractModule with ScalaModule {
    override def configure(): Unit = {
      bind[ActorFactory].annotatedWith(Names.named(classOf[UserService])).toProvider[ApplicationModule.UserServiceProvider]
    }
  }

}

