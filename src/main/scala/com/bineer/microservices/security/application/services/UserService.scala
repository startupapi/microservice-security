package com.bineer.microservices.security.application.services

import akka.actor.{Actor, ActorLogging, Props}
import akka.pattern.{ask, pipe}
import com.bineer.microservices.security.SystemInjector
import com.bineer.microservices.security.domain.aggregates.UserProtocol._
import com.bineer.microservices.security.domain.repositories.UserRepository
import com.bineer.microservices.security.domain.util.UserParameter
import com.bineer.microservices.security.infrastructure.dao.UserMysqlDao
import com.bineer.microservices.security.infrastructure.to.{UserMapper, UserTO}
import com.scalakka.microservices.kernel.annotation.Names
import com.scalakka.microservices.kernel.domain.AggregateRepositoryProtocol.{CreateAggregate, FindByIdAggregate}
import com.scalakka.microservices.kernel.util.ActorFactoryType.ActorFactory
import com.scalakka.microservices.kernel.util.Result._
import com.scalakka.microservices.kernel.util.Timeout.Implicit._
import com.scalakka.microservices.kernel.util._
import net.codingwell.scalaguice.InjectorExtensions._
import org.springframework.data.domain.{Page, PageImpl}

import scala.collection.JavaConversions._
import scala.concurrent.Future
import scala.util.Try
import scala.util.{Success => TrySuccess, Failure => TryFailure}

class UserService extends Actor with ActorLogging {

  import UserServiceMessageProtocol._
  import UserServiceProtocol._

  val userRepositoryFactory: ActorFactory = SystemInjector.injector.instance[ActorFactory](Names.named(classOf[UserRepository]))
  val userDao: UserMysqlDao = SystemInjector.injector.instance[UserMysqlDao]

  override def receive: Receive = {
    case Save(data: UserParameter) => save(data: UserParameter) pipeTo sender()
    case Update(id, data: UserParameter) => update(id, data)
    case Delete(id) => delete(id) pipeTo sender()
    case FindAll(pageRequest) => sender() ! findAll(pageRequest)
    case FindById(id, _) => sender() ! findById(id)
    case Login(email, password) => sender() ! login(email, password)
    case ChangePassword(email, oldPassword, newPassword) => changePassword(email, oldPassword, newPassword) pipeTo sender()
  }

  def save(data: UserParameter): AsyncResult[DataIdentity] = Option(userDao.findByEmail(data.email)) match {
    case None => (userRepositoryFactory() ? CreateAggregate(CreateUserCommand(IdentifierGenerator.generate, data))).mapResult[DataIdentity]
    case _ => Future(Failure(List(EmailUserRegisteredError)))
  }

  def update(id: String, data: UserParameter): AsyncResult[DataIdentity] = {
    (userRepositoryFactory() ? FindByIdAggregate(UpdateUserCommand(id, data))).mapResult[DataIdentity]
  }

  def delete(id: String): AsyncResult[DataIdentity] = {
    (userRepositoryFactory() ? FindByIdAggregate(DeleteUserCommand(id))).mapResult[DataIdentity]
  }

  def findAll(pageRequest: PageRequest): Result[Page[UserMapper]] = {
    try {
      userDao.findAll(pageRequest) match {
        case p: Page[UserTO] => Result(new PageImpl(p.getContent.toList.map(_.toMapper), pageRequest, p.getSize))
        case _ => Result(new PageImpl(List()))
      }
    }
    catch {
      case e: Throwable =>
        log.error(e.getMessage)
        Failure(List(OperationDaoError))
    }
  }

  def findById(id: String): Result[Option[UserMapper]] = {
    try {
      Option(userDao.findByUserId(id)) match {
        case Some(user) => Result(Some(user.toMapper))
        case _ => Failure(List(NotFoundEmailUserError))
      }
    }
    catch {
      case e: Throwable =>
        log.error(e.getMessage)
        Failure(List(OperationDaoError))
    }
  }

  def login(email: String, password: String): Result[Option[UserMapper]] = Try(Option(userDao.findByEmail(email))) match {
    case TrySuccess(Some(user)) if user.password == password => Success(Some(user.toMapper))
    case TrySuccess(_) => Failure(List(IncorrectEmailOrPasswordUserError))
    case TryFailure(e) =>
      log.error(e.getMessage)
      Failure(List(OperationDaoError))
  }

  def changePassword(email: String, oldPassword: String, newPassword: String): AsyncResult[Boolean] = {
    try {
      Option(userDao.findByEmail(email)) match {
        case Some(user) if user.password != oldPassword => Future(Failure(List(NotEqualOldPasswordError)))
        case Some(user) => (userRepositoryFactory() ? FindByIdAggregate(ChangePasswordCommand(user.userId, newPassword))).mapResult[Boolean]
        case _ => Future(Failure(List(NotFoundEmailUserError)))
      }
    }
    catch {
      case e: Throwable =>
        log.error(e.getMessage)
        Future(Failure(List(OperationDaoError)))
    }
  }
}

object UserService {
  def props = Props(classOf[UserService])
}

object UserServiceMessageProtocol {

  case object EmailUserRegisteredError extends Error

  case object NotFoundEmailUserError extends Error

  case object IncorrectEmailOrPasswordUserError extends Error

  case object NotEqualOldPasswordError extends Error

  case object OperationDaoError extends Error

}

object UserServiceProtocol extends CrudProtocol {

  case class Login(email: String, password: String)

  case class ChangePassword(email: String, oldPassword: String, newPassword: String)

}
