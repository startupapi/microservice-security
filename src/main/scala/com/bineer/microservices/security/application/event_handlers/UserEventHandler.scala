package com.bineer.microservices.security.application.event_handlers

import com.bineer.microservices.security.SystemInjector
import com.bineer.microservices.security.domain.aggregates.UserProtocol._
import com.bineer.microservices.security.domain.util.UserParameter
import com.bineer.microservices.security.infrastructure.dao.UserMysqlDao
import com.bineer.microservices.security.infrastructure.to.UserTO
import com.scalakka.microservices.kernel.application.event_handlers.EventHandler
import com.scalakka.microservices.kernel.domain.AggregateRootProtocol.IdentifiedEvent
import com.scalakka.microservices.kernel.util.StringUtil
import net.codingwell.scalaguice.InjectorExtensions._

class UserEventHandler extends EventHandler {

  val userDao: UserMysqlDao = SystemInjector.injector.instance[UserMysqlDao]

  override val eventMatching: PartialFunction[IdentifiedEvent, Unit] = {
    case UserCreatedEvent(id, data) => createUser(id, data)
    case UserUpdatedEvent(id, data) => updateUser(id, data)
    case UserDeletedEvent(id) => deleteUser(id)
    case UserChangedPasswordEvent(id, newPassword) => changePasswordUser(id, newPassword)
  }

  def createUser(id: String, data: UserParameter): Unit = {
    userDao.save(new UserTO(id, data.firstName, data.lastName, data.email, getUsernameByEmail(data.email), StringUtil.md5(data.password)))
  }


  def getUsernameByEmail(email: String): String = email.split("@").toList match {
    case Nil => ""
    case head :: _ => head
  }

  def updateUser(id: String, data: UserParameter): Unit = Option(userDao.findByUserId(id)) match {
    case Some(user) =>
      user.firstName = data.firstName
      user.lastName = data.lastName
      userDao.save(user)
    case None =>
  }

  def deleteUser(id: String): Unit = Option(userDao.findByUserId(id)) match {
    case Some(user) => userDao.delete(user)
    case None =>
  }

  def changePasswordUser(id: String, newPassword: String): Unit = Option(userDao.findByUserId(id)) match {
    case Some(user) =>
      user.password = newPassword
      userDao.save(user)
    case None =>
  }
}
