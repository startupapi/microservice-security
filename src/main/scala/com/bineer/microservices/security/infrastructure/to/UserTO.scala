package com.bineer.microservices.security.infrastructure.to

import javax.persistence._
import scala.beans.BeanProperty

@Entity
@Table(name = "user")
class UserTO(ui: String, f: String, l: String, e: String, u: String, p: String) {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @BeanProperty
  var id: Long = _

  @BeanProperty
  @Column(name = "user_id_ref")
  var userId: String = ui

  @BeanProperty
  @Column(name = "first_name")
  var firstName: String = f

  @BeanProperty
  @Column(name = "last_name")
  var lastName: String = l

  @Column(name = "email")
  @BeanProperty
  var email: String = e

  @Column(name = "username")
  @BeanProperty
  var username: String = u

  @Column(name = "password")
  @BeanProperty
  var password: String = p

  def this() = this("", "", "", "", "", "")

  def toMapper = UserMapper(id, userId, firstName, lastName, email, username)

}

case class UserMapper(id: Long, userIdRef: String, firstName: String, lastName: String, email: String, username: String)

