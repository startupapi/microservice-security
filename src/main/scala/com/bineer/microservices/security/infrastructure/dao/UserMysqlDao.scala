package com.bineer.microservices.security.infrastructure.dao

import com.bineer.microservices.security.infrastructure.to.UserTO
import org.springframework.data.jpa.repository.JpaRepository

trait UserMysqlDao extends JpaRepository[UserTO, String] {

  def findByUserId(userId: String): UserTO

  def findByEmail(email: String): UserTO
}
