package com.bineer.microservices.security.infrastructure

import com.google.inject.{AbstractModule, Provider}
import com.scalakka.microservices.kernel.util.{EnvironmentConfig, StringUtil}
import com.bineer.microservices.security.infrastructure.dao.UserMysqlDao
import net.codingwell.scalaguice.ScalaModule
import org.springframework.context.ApplicationContext

package object ioc {

  object InfrastructureModule {
    private val context: ApplicationContext = EnvironmentConfig.getApplicationContext("security-repository-jpa")

    class UserMysqlDaoProvider() extends Provider[UserMysqlDao] {
      override def get(): UserMysqlDao = {
        context.getBean(StringUtil.decapitalize(classOf[UserMysqlDao])).asInstanceOf[UserMysqlDao]
      }
    }

  }

  class InfrastructureModule extends AbstractModule with ScalaModule {
    override def configure(): Unit = {
      bind[UserMysqlDao].toProvider[InfrastructureModule.UserMysqlDaoProvider]
    }
  }

}
