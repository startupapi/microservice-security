package com.bineer.microservices.security

import com.google.inject.Injector
import com.scalakka.microservices.kernel.application.schedulers.Scheduler
import com.scalakka.microservices.kernel.domain.AggregateRepository
import com.scalakka.microservices.kernel.util.{ApiVersion, BaseMain, Handler, RouteResource}
import com.bineer.microservices.security.api.controllers.v1.UserController
import com.bineer.microservices.security.application.event_handlers.UserEventHandler
import com.bineer.microservices.security.domain.aggregates.UserProtocol.{UserChangedPasswordEvent, UserCreatedEvent, UserDeletedEvent, UserUpdatedEvent}
import com.bineer.microservices.security.domain.repositories.UserRepository
import com.bineer.microservices.security.util.RoutePathEnum._

object Main extends BaseMain with ApiVersion {

  override val injector: Injector = SystemInjector.injector

  override val routes: Seq[RouteResource] = Seq(
    RouteResource(USERS) controller Map(VERSION_V1 -> classOf[UserController])
  )

  override val aggregateRepositories: Seq[Class[_ <: AggregateRepository[_]]] = Seq(
    classOf[UserRepository]
  )

  override val eventHandlers: Seq[Handler] = Seq(
    Handler(classOf[UserEventHandler]) events Seq(classOf[UserCreatedEvent], classOf[UserUpdatedEvent], classOf[UserDeletedEvent], classOf[UserChangedPasswordEvent])
  )

  override val schedulers: Seq[Class[_ <: Scheduler]] = Seq()
}
