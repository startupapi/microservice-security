package com.bineer.microservices.security.domain.aggregates

import com.bineer.microservices.security.domain.util.UserParameter
import com.scalakka.microservices.kernel.domain.AggregateRoot
import com.scalakka.microservices.kernel.domain.AggregateRootProtocol._
import com.scalakka.microservices.kernel.util.Result.Error
import com.scalakka.microservices.kernel.util._

class User extends AggregateRoot {

  import UserMessageProtocol._
  import UserProtocol._

  override def validateCommand: PartialFunction[Command, Result[AggregateRootResponse]] = {
    case CreateUserCommand(id, data) => createUser(id, data, validateFields(data, isUpdate = false))
    case UpdateUserCommand(id, data) => updateUser(id, data, validateFields(data))
    case DeleteUserCommand(id) => Success(AggregateRootResponse(DataIdentity(id), Some(UserDeletedEvent(id))))
    case ChangePasswordCommand(id, newPassword) => Success(AggregateRootResponse(true, Some(UserChangedPasswordEvent(id, newPassword))))
  }

  def createUser(id: String, data: UserParameter, listError: List[Error]): Result[AggregateRootResponse] = listError match {
    case Nil => Success(AggregateRootResponse(DataIdentity(id), Some(UserCreatedEvent(id, data))))
    case _ => Failure(listError)
  }

  def updateUser(id: String, data: UserParameter, listError: List[Error]): Result[AggregateRootResponse] = listError match {
    case Nil => Success(AggregateRootResponse(DataIdentity(id), Some(UserUpdatedEvent(id, data))))
    case _ => Failure(listError)
  }

  def validateFields(data: UserParameter, isUpdate: Boolean = true): List[Error] = {
    var listError = List.empty[Error]
    if (data.firstName.isEmpty) listError = listError ::: List(FirstNameIsEmpty)
    if (data.lastName.isEmpty) listError = listError ::: List(LastNameIsEmpty)
    if ((data.email.isEmpty || data.email.isEmpty) && !isUpdate) listError = listError ::: List(EmailIsEmpty)
    listError
  }
}

object UserMessageProtocol {

  case object FirstNameIsEmpty extends Error

  case object LastNameIsEmpty extends Error

  case object EmailIsEmpty extends Error

}

object UserProtocol {

  case class CreateUserCommand(aggregateId: String, data: UserParameter) extends CreateCommand

  case class UpdateUserCommand(aggregateId: String, data: UserParameter) extends IdentifiedCommand

  case class ChangePasswordCommand(aggregateId: String, newPassword: String) extends IdentifiedCommand

  case class DeleteUserCommand(aggregateId: String) extends DeleteCommand

  case class UserCreatedEvent(id: String, data: UserParameter) extends CreatedEvent

  case class UserUpdatedEvent(id: String, data: UserParameter) extends UpdatedEvent

  case class UserChangedPasswordEvent(id: String, newPassword: String) extends UpdatedEvent

  case class UserDeletedEvent(id: String) extends DeletedEvent

}
