package com.bineer.microservices.security.domain.repositories

import com.scalakka.microservices.kernel.domain.AggregateRepository
import com.bineer.microservices.security.domain.aggregates.User

class UserRepository extends AggregateRepository[User]
