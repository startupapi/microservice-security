package com.bineer.microservices.security.domain

import com.google.inject._
import com.scalakka.microservices.kernel.annotation.{Named, Names}
import com.scalakka.microservices.kernel.application.ServiceProvider
import com.scalakka.microservices.kernel.util.ActorFactoryType.ActorFactory
import com.scalakka.microservices.kernel.util.ClusterShardingManager
import com.bineer.microservices.security.domain.repositories.UserRepository
import net.codingwell.scalaguice.ScalaModule

 package object  ioc {

   object DomainModule {

     @Named(classOf[UserRepository])
     class UserRepositoryProvider() extends Provider[ActorFactory] with ServiceProvider {
       override def get(): ActorFactory = () => ClusterShardingManager.getActor(classOf[UserRepository])
     }

   }

   class DomainModule extends AbstractModule with ScalaModule {
     override def configure(): Unit = {
       bind[ActorFactory].annotatedWith(Names.named(classOf[UserRepository])).toProvider[DomainModule.UserRepositoryProvider]
     }
   }
}
