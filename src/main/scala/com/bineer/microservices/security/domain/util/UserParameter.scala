package com.bineer.microservices.security.domain.util

import com.scalakka.microservices.kernel.api.Parameter

case class UserParameter(firstName: String,
                         lastName: String,
                         email: String,
                         password: String) extends Parameter

