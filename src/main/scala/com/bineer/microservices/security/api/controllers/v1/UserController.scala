package com.bineer.microservices.security.api.controllers.v1

import javax.inject.Inject

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._
import akka.pattern.ask
import com.bineer.microservices.security.application.services.UserService
import com.bineer.microservices.security.application.services.UserServiceProtocol._
import com.bineer.microservices.security.domain.util.UserParameter
import com.bineer.microservices.security.infrastructure.to.{UserTO, UserMapper}
import com.bineer.microservices.security.util.RoutePathEnum._
import com.scalakka.microservices.kernel.annotation.Named
import com.scalakka.microservices.kernel.api._
import com.scalakka.microservices.kernel.util.ActorFactoryType.ActorFactory
import com.scalakka.microservices.kernel.util.Result._
import com.scalakka.microservices.kernel.util.Timeout.Implicit._
import com.scalakka.microservices.kernel.util.{DataIdentity, StringUtil}
import org.springframework.data.domain.Page

case class UserLoginData(email: String, password: String) extends Parameter

case class UserChangePasswordData(email: String, oldPassword: String, newPassword: String) extends Parameter

class UserController @Inject()(@Named(classOf[UserService]) userServiceFactory: ActorFactory)
  extends ResourceController {

  override def post: Route = Action.postAsync {
    (data: UserParameter, _) =>
      (userServiceFactory() ? Save(data)).mapResult[DataIdentity]
  }

  override def put: Route = Action.putAsync {
    (id: String, data: UserParameter, _) =>
      (userServiceFactory() ? Update(id, data)).mapResult[DataIdentity]
  }

  override def delete: Route = Action.deleteAsync {
    (id: String, _) =>
      (userServiceFactory() ? Delete(id)).mapResult[DataIdentity]
  }

  override def getById: Route = Action.getAsync {
    (id: String, r: Request) =>
      (userServiceFactory() ? FindById(id, r.pageRequest)).mapResult[Option[UserTO]]
  }

  override def getAll: Route = Action.getAsync {
    (r: Request) =>
      (userServiceFactory() ? FindAll(r.pageRequest)).mapResult[Page[UserTO]]
  }

  def login: Route = Action.custom {
    path(LOGIN) {
      Action.postAsync {
        (data: UserLoginData, _: Request) =>
          (userServiceFactory() ? Login(data.email, StringUtil.md5(data.password))).mapResult[UserMapper]
      }
    }
  }

  def changePassword: Route = Action.custom {
    path(CHANGE_PASSWORD) {
      Action.postAsync {
        (data: UserChangePasswordData, _: Request) =>
          (userServiceFactory() ? ChangePassword(data.email, StringUtil.md5(data.oldPassword), StringUtil.md5(data.newPassword))).mapResult[Boolean]
      }
    }
  }

  override def routes: Route = login ~ changePassword ~ super.routes

}